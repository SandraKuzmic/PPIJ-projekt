package jawas.ppij.fer.hr.trainme.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jawas.ppij.fer.hr.trainme.Constants;
import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.Utils;
import jawas.ppij.fer.hr.trainme.adapter.RecyclerViewAdapter;
import jawas.ppij.fer.hr.trainme.adapter.SwipeAdapter;
import jawas.ppij.fer.hr.trainme.databinding.ActivityMainBinding;
import jawas.ppij.fer.hr.trainme.databinding.FragmentDailyWorkoutBinding;
import jawas.ppij.fer.hr.trainme.databinding.FragmentWorkoutsBinding;
import jawas.ppij.fer.hr.trainme.entity.Workout;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class MainActivity extends AppCompatActivity {

    private static final int DAILY_WORKOUTS_NUMBER = 5;
    private ActivityMainBinding mBinding;
    private int level;
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
        level = preferences.getInt(getString(R.string.pref_level), Level.DEFAULT_VALUE);

        JodaTimeAndroid.init(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        adapter = new RecyclerViewAdapter(this, getWorkouts(), new RecyclerViewAdapter.OnWorkoutClickListener() {
            @Override
            public void onWorkoutClick(int position) {
                openWorkout(getWorkouts().get(position));
            }
        });

        mBinding.executePendingBindings();

        setupFragment();
    }

    private void setupFragment() {
        SwipeAdapter adapter = new SwipeAdapter(getSupportFragmentManager(), this);

        mBinding.pager.setAdapter(adapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.pager);
    }

    private void openWorkout(Workout workout) {
        Intent intent = new Intent(this, WorkoutActivity.class);
        intent.putExtra(getString(R.string.workout_intent), workout);
        startActivityForResult(intent, Constants.WORKOUT_REQUEST_CODE);
    }

    private List<Workout> getWorkouts() {
        return Utils.getWorkouts(MainActivity.this, level);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.WORKOUT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                checkAccomplishments();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.level_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.level_info:
                startActivity(new Intent(this, InfoActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void checkAccomplishments() {
//        Database db = new Database(this);
//        List<Workout> listOfWorkouts = db.getWorkoutsByLevel(level);
        List<Workout> listOfWorkouts = getWorkouts();

        if (Level.valueOf(level) == Level.ADVANCED) {
            adapter.updateAdapter(listOfWorkouts);
            return;
        }

        boolean levelAccomplished = true;
        for (Workout workout : listOfWorkouts) {
            // If exist a workout of this level with no goal acc, break out
            if (workout.getLevel().getValue() == level && !workout.isGoalAccomplished()) {
                levelAccomplished = false;
                break;
            }
        }

        if (levelAccomplished) {
            level++;
            SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
            preferences.edit().putInt(getString(R.string.pref_level), level).apply();

            String newLevel = Utils.capitalizeLetter(Level.valueOf(level).name());
            Snackbar.make(mBinding.getRoot(), getString(R.string.new_level, newLevel), Snackbar.LENGTH_LONG).show();
        }

        adapter.updateAdapter(getWorkouts());
    }

    public void setWorkoutsList(FragmentWorkoutsBinding binding) {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
    }

    public List<Workout> getDailyWorkouts() {
        int currIndex;
        Calendar calendar = Calendar.getInstance();
        currIndex = calendar.get(Calendar.DAY_OF_WEEK);
        List<Workout> listOfWrkts = getWorkouts();
        List<Workout> listForReturn = new ArrayList<>();
        for (int i = 0; i < DAILY_WORKOUTS_NUMBER; ++i) {
            if (currIndex >= listOfWrkts.size()) {
                while (true) {
                    Workout wrkt = listOfWrkts.get(currIndex % listOfWrkts.size());
                    if (listForReturn.contains(wrkt)) {
                        currIndex++;
                        continue;
                    }
                    listForReturn.add(wrkt);
                    break;
                }
            } else
                listForReturn.add(listOfWrkts.get(currIndex));
            currIndex += calendar.get(Calendar.DAY_OF_WEEK);
        }
        return listForReturn;
    }

    public void setDailyWorkout(FragmentDailyWorkoutBinding binding) {

        final List<Workout> workouts = getDailyWorkouts();

        binding.cvDailyWorkout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWorkout(workouts.get(0));
            }
        });
        setWorkoutText(binding.dailyWorkout1, binding.dailyWorkout1Reps, workouts.get(0));

        binding.cvDailyWorkout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWorkout(workouts.get(1));
            }
        });
        setWorkoutText(binding.dailyWorkout2, binding.dailyWorkout2Reps, workouts.get(1));

        binding.cvDailyWorkout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWorkout(workouts.get(2));
            }
        });
        setWorkoutText(binding.dailyWorkout3, binding.dailyWorkout3Reps, workouts.get(2));

        binding.cvDailyWorkout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWorkout(workouts.get(3));
            }
        });
        setWorkoutText(binding.dailyWorkout4, binding.dailyWorkout4Reps, workouts.get(3));

        binding.cvDailyWorkout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWorkout(workouts.get(4));
            }
        });
        setWorkoutText(binding.dailyWorkout5, binding.dailyWorkout5Reps, workouts.get(4));

        //setup checkbox
        final SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
        String saved = preferences.getString(getString(R.string.pref_daily_workout), "");
        if (!saved.equals("")) {
            DateTime today = DateTime.now().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfDay(0);
            if (today.toString().equals(saved)) {
                //set from prefs
                boolean defaultCompleted = getResources().getBoolean(R.bool.default_completed);
                binding.checkboxCompleted.setChecked(preferences.getBoolean(getString(R.string.pref_daily_workout_completed), defaultCompleted));
            } else {
                //reset checkbox
                binding.checkboxCompleted.setChecked(false);
                preferences.edit().putBoolean(getString(R.string.pref_daily_workout_completed), false).apply();
            }
        }

        binding.checkboxCompleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DateTime dateTime = DateTime.now().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfDay(0);
                preferences.edit()
                        .putBoolean(getString(R.string.pref_daily_workout_completed), isChecked)
                        .putString(getString(R.string.pref_daily_workout), dateTime.toString())
                        .apply();
            }
        });
    }

    private void setWorkoutText(TextView dailyWorkout, TextView reps, Workout workout) {
        dailyWorkout.setText(workout.getName());
        reps.setText(getString(R.string.daily_reps, workout.getGoalReps()));
    }
}


