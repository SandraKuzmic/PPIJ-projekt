package jawas.ppij.fer.hr.trainme.activity;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import jawas.ppij.fer.hr.trainme.Constants;
import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.Utils;
import jawas.ppij.fer.hr.trainme.databinding.ActivityWorkoutBinding;
import jawas.ppij.fer.hr.trainme.db.Database;
import jawas.ppij.fer.hr.trainme.entity.Workout;

import static jawas.ppij.fer.hr.trainme.Utils.extractUrlId;
import static jawas.ppij.fer.hr.trainme.Utils.isConnectedToInternet;

public class WorkoutActivity extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {
    private static final int RESET_CONSTANT = 0;
    private Workout workout;
    private Toast mToast;

    private YouTubePlayerSupportFragment fragment;

    private ActivityWorkoutBinding mBinding;

    private Database db = new Database(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_workout);

        workout = (Workout) this.getIntent().getSerializableExtra(getString(R.string.workout_intent));

        mBinding.content.currentLevel.setText(workout.getLevel().toString());
        mBinding.content.workoutName.setText(workout.getName());
        mBinding.content.personalBest.setText(getString(R.string.reps, workout.getCurrentReps(), workout.getGoalReps()));
        mBinding.content.personalBest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRepsDialog();
            }
        });
        mBinding.content.workoutCategory.setText(Utils.capitalizeLetter(workout.getCategory().toString()));
        setUpWeightView();
        mBinding.content.workoutDescription.setText(workout.getDescription());
        mBinding.content.workoutUrl.setText(workout.getUrl());

        fragment = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);

        if (isConnectedToInternet(this)) {
            showVideo();
        } else {
            hideVideo();
        }

        mBinding.inputWorkout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showRepsDialog();
            }
        });

        mBinding.executePendingBindings();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.workout_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.workout_reset:
                resetReps();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void resetReps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.reset_reps));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                workout.setCurrentReps(RESET_CONSTANT);
                mBinding.content.personalBest.setText(getString(R.string.reps, workout.getCurrentReps(), workout.getGoalReps()));
                db.insertNewRepetitions(workout.getId(), RESET_CONSTANT, workout.getLevel());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    private void showRepsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.current_reps_choose));
        final EditText txtReps = new EditText(this);
        txtReps.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        builder.setView(txtReps);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String inputText = txtReps.getText().toString();
                int reps = Integer.parseInt(inputText);
                if (reps <= workout.getCurrentReps()) {
                    mToast = Utils.addToast(mToast, WorkoutActivity.this, R.string.current_reps_lower, true);
                    return;
                }
                workout.setCurrentReps(reps);
                mBinding.content.personalBest.setText(getString(R.string.reps, workout.getCurrentReps(), workout.getGoalReps()));
                db.insertNewRepetitions(workout.getId(), reps, workout.getLevel());
                mToast = Utils.addToast(mToast, WorkoutActivity.this, R.string.current_reps_more, true);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            String video = extractUrlId(workout.getUrl());
            if (video != null) {
                youTubePlayer.cueVideo(video);
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
        mToast = Utils.addToast(mToast, this, error.toString(), true);
    }

    private void setUpWeightView() {
        if (workout.getWeight() == 0) {
            mBinding.content.labelWeights.setVisibility(View.GONE);
            mBinding.content.workoutWeight.setVisibility(View.GONE);
        } else {
            mBinding.content.workoutWeight.setText(getString(R.string.weight_kg, workout.getWeight()));
        }
    }

    private void showVideo() {
        mBinding.content.workoutUrl.setVisibility(View.GONE);
        fragment.initialize(Constants.API_KEY, this);
    }

    private void hideVideo() {
        fragment.getView().setVisibility(View.GONE);
    }
}
