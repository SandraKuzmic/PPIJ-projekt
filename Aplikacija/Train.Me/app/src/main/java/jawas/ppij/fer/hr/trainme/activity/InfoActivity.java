package jawas.ppij.fer.hr.trainme.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.Utils;
import jawas.ppij.fer.hr.trainme.databinding.ActivityInfoBinding;
import jawas.ppij.fer.hr.trainme.entity.Workout;
import jawas.ppij.fer.hr.trainme.enums.Category;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class InfoActivity extends AppCompatActivity {

    private int level;
    private ActivityInfoBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_info);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
        level = preferences.getInt(getString(R.string.pref_level), Level.DEFAULT_VALUE);
        mBinding.userLevel.setText(Level.valueOf(level).toString());

        Map<Category, Double> map = getStatistics();

        mBinding.chestPercentage.setText(getString(R.string.percentage, map.get(Category.CHEST).intValue()));

        setTextToView(mBinding.armsPercentage, map.get(Category.ARMS).intValue());
        setTextToView(mBinding.chestPercentage, map.get(Category.CHEST).intValue());
        setTextToView(mBinding.absPercentage, map.get(Category.ABS).intValue());
        setTextToView(mBinding.backPercentage, map.get(Category.BACK).intValue());
        setTextToView(mBinding.legsPercentage, map.get(Category.LEGS).intValue());
        setTextToView(mBinding.cardioPercentage, map.get(Category.CARDIO).intValue());
        setTextToView(mBinding.overallPercentage, (int) getOverallStatistics());
    }

    private void setTextToView(TextView textView, int value) {
        if (value == 100) {
            textView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        textView.setText(getString(R.string.percentage, value));
    }

    public Map<Category, Double> getStatistics() {
        Map<Category, Double> mapOfStatistics = new HashMap<>();
        List<Workout> listOfWrkts = Utils.getWorkouts(this, level);
        for (Category category : Category.values()) {
            mapOfStatistics.put(category, (double) 0);
            int sumOfReps = 0;
            int totalSum = 0;
            for (Workout workout : listOfWrkts) {
                if (workout.getCategory().equals(category)) {
                    if (workout.getCurrentReps() >= workout.getGoalReps())
                        sumOfReps += workout.getGoalReps();
                    else
                        sumOfReps += workout.getCurrentReps();
                    totalSum += workout.getGoalReps();
                }
            }
            mapOfStatistics.put(category, (sumOfReps / (double) totalSum) * 100);
        }
        return mapOfStatistics;
    }

    public double getOverallStatistics() {
        List<Workout> listOfWrkts = Utils.getWorkouts(this, level);
        int sumOfReps = 0;
        int totalSum = 0;
        for (Workout workout : listOfWrkts) {
            totalSum += workout.getGoalReps();
            if (workout.getCurrentReps() > workout.getGoalReps())
                sumOfReps += workout.getGoalReps();
            else
                sumOfReps += workout.getCurrentReps();
        }
        return (sumOfReps / (double) totalSum) * 100;
    }

}
