package jawas.ppij.fer.hr.trainme.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //check if level has been selected
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
        int level = preferences.getInt(getString(R.string.pref_level), Level.DEFAULT_VALUE);
        if (level == Level.DEFAULT_VALUE) {
            startActivity(new Intent(this, LevelActivity.class));
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }
}
