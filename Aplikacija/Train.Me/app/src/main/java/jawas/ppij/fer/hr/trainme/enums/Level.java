package jawas.ppij.fer.hr.trainme.enums;

import java.util.HashMap;
import java.util.Map;

public enum Level {
    BEGINNER(0), INTERMEDIATE(1), ADVANCED(2);

    public static final int DEFAULT_VALUE = -1;

    private int value;
    private static Map<Integer, Level> map = new HashMap<>();

    static {
        for (Level level : Level.values()) {
            map.put(level.value, level);
        }
    }

    Level(int i) {
        value = i;
    }

    public int getValue() {
        return value;
    }

    public static Level valueOf(int level) {
        return map.get(level);
    }
}