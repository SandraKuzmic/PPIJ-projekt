package jawas.ppij.fer.hr.trainme.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.databinding.RvItemBinding;
import jawas.ppij.fer.hr.trainme.entity.Workout;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<Workout> workouts;
    private OnWorkoutClickListener listener;

    public RecyclerViewAdapter(Context context, List<Workout> workouts, OnWorkoutClickListener listener) {
        this.context = context;
        this.workouts = workouts;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RvItemBinding binding = RvItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(context, binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(listener, workouts.get(position));
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    public void updateAdapter(List<Workout> workouts) {
        this.workouts = workouts;
        notifyDataSetChanged();
    }

    public interface OnWorkoutClickListener {
        void onWorkoutClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private RvItemBinding binding;

        ViewHolder(Context context, RvItemBinding binding) {
            super(binding.getRoot());
            this.context = context;
            this.binding = binding;
        }

        void bind(final OnWorkoutClickListener listener, Workout workout) {
            int background = -1;
            switch (workout.getLevel()) {
                case BEGINNER:
                    background = R.drawable.border_left_beginner;
                    break;
                case INTERMEDIATE:
                    background = R.drawable.border_left_intermediate;
                    break;
                case ADVANCED:
                    background = R.drawable.border_left_advanced;
                    break;
            }

            binding.workoutLayout.setBackground(ContextCompat.getDrawable(context, background));
            binding.tvWorkoutName.setText(workout.getName());
            binding.tvWorkoutReps.setText(context.getString(R.string.reps, workout.getCurrentReps(), workout.getGoalReps()));
            binding.workoutStar.setVisibility(workout.isGoalAccomplished() ? View.VISIBLE : View.INVISIBLE);
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onWorkoutClick(getLayoutPosition());
                }
            });
            binding.executePendingBindings();
        }
    }
}
