package jawas.ppij.fer.hr.trainme.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import jawas.ppij.fer.hr.trainme.entity.Workout;
import jawas.ppij.fer.hr.trainme.enums.Category;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class Database {
    private static DatabaseOpenHelper databaseOpenHelper = null;
    private SQLiteDatabase dbWritable;
    private SQLiteDatabase dbReadable;
    private Context context;

    public Database(Context context) {
        this.context = context;
        databaseOpenHelper = DatabaseOpenHelper.getInstance(context, databaseOpenHelper);
        dbWritable = databaseOpenHelper.getWritableDatabase();
        dbReadable = databaseOpenHelper.getReadableDatabase();
    }

    public boolean addWorkout(Workout workout) {
        ContentValues exerciseValues = new ContentValues();
        exerciseValues.put(Constants.EXERCISE_NAME, workout.getName());
        exerciseValues.put(Constants.EXERCISE_DESCRIPTION, workout.getDescription());
        exerciseValues.put(Constants.EXERCISE_URL, workout.getUrl());

        boolean insertionSuccess;
        insertionSuccess = dbWritable.insert(Constants.DATABASE_EXERCISE_TABLE_NAME, null, exerciseValues) != -1;
        if (!insertionSuccess)
            return false;

        String sqlQuery = "select " + Constants.EXERCISE_ID +
                " from " + Constants.DATABASE_EXERCISE_TABLE_NAME +
                " where " + Constants.EXERCISE_NAME + " = '" + workout.getName() +
                "' and " + Constants.EXERCISE_DESCRIPTION + " = '" + workout.getDescription() + "';";
        Cursor cursor = dbReadable.rawQuery(sqlQuery, null);
        int id = -1;
        if (cursor.moveToFirst())
            id = cursor.getInt(0);
        if (id == -1)
            return false;

        ContentValues statusValues = new ContentValues();
        statusValues.put(Constants.STATUS_EXERCISE_ID, id);
        statusValues.put(Constants.STATUS_LEVEL, workout.getLevel().ordinal());
        statusValues.put(Constants.STATUS_CATEGORY, workout.getCategory().toString());
        statusValues.put(Constants.STATUS_WEIGHT, workout.getWeight());
        statusValues.put(Constants.STATUS_CURRENT_REPETITION, workout.getCurrentReps());
        statusValues.put(Constants.STATUS_NEEDED_REPETITION, workout.getGoalReps());

        insertionSuccess = dbWritable.insert(Constants.DATABASE_STATUS_TABLE_NAME, null, statusValues) != -1;
        System.out.println(insertionSuccess + "\n" + workout.toString());
        return insertionSuccess;
    }

    public List<Workout> getAllWorkouts() {
        String sqlQuery = "select * from " + Constants.DATABASE_EXERCISE_TABLE_NAME + ", " + Constants.DATABASE_STATUS_TABLE_NAME +
                " where " + Constants.DATABASE_EXERCISE_TABLE_NAME + "." + Constants.EXERCISE_ID + "=" + Constants.STATUS_EXERCISE_ID + ";";
        Cursor cursor = dbReadable.rawQuery(sqlQuery, null);
        List<Workout> returnList = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            returnList.add(new Workout(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_EXERCISE_ID))),  //id
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_NAME)),                    //name
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_DESCRIPTION)),                    //description
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_URL)),                    //url
                    Level.valueOf(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_LEVEL)))),  //level
                    Category.valueOf(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CATEGORY))),  //category
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_WEIGHT))),  //weight
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CURRENT_REPETITION))),  //current repetitions
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_NEEDED_REPETITION))) //needed repetitions
            ));
            cursor.moveToNext();
        }
        return returnList;
    }

    public List<Workout> getWorkoutsByLevel(Level level) {
        return getWorkoutsByLevel(level.ordinal());
    }
    public List<Workout> getWorkoutsByLevel(int level) {
        String sqlQuery = "select * from " + Constants.DATABASE_EXERCISE_TABLE_NAME + ", " + Constants.DATABASE_STATUS_TABLE_NAME +
                " where " + Constants.DATABASE_EXERCISE_TABLE_NAME + "." + Constants.EXERCISE_ID + "=" + Constants.STATUS_EXERCISE_ID +
                " and " + Constants.STATUS_LEVEL + " <= " + level
                + " order by " + Constants.STATUS_CATEGORY
                + ";";
        Cursor cursor = dbReadable.rawQuery(sqlQuery, null);
        List<Workout> returnList = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            returnList.add(new Workout(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_EXERCISE_ID))),  //id
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_NAME)),                    //name
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_DESCRIPTION)),                    //description
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_URL)),                    //url
                    Level.valueOf(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_LEVEL)))),  //level
                    Category.valueOf(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CATEGORY))),  //category
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_WEIGHT))),  //weight
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CURRENT_REPETITION))),  //current repetitions
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_NEEDED_REPETITION))) //needed repetitions
            ));
            cursor.moveToNext();
        }
        return returnList;
    }

    public List<Workout> getWorkoutsByCategory(Category category) {
        String sqlQuery = "select * from " + Constants.DATABASE_EXERCISE_TABLE_NAME + ", " + Constants.DATABASE_STATUS_TABLE_NAME +
                " where " + Constants.DATABASE_EXERCISE_TABLE_NAME + "." + Constants.EXERCISE_ID + "=" + Constants.STATUS_EXERCISE_ID +
                " and " + Constants.STATUS_CATEGORY + " = '" + category.toString() + "';";
        Cursor cursor = dbReadable.rawQuery(sqlQuery, null);
        List<Workout> returnList = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            returnList.add(new Workout(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_EXERCISE_ID))),  //id
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_NAME)),                    //name
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_DESCRIPTION)),                    //description
                    cursor.getString(cursor.getColumnIndex(Constants.EXERCISE_URL)),                    //url
                    Level.valueOf(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_LEVEL)))),  //level
                    Category.valueOf(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CATEGORY))),  //category
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_WEIGHT))),  //weight
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_CURRENT_REPETITION))),  //current repetitions
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.STATUS_NEEDED_REPETITION))) //needed repetitions
            ));
            cursor.moveToNext();
        }
        return returnList;
    }

    public void insertNewRepetitions(int id, int repetitions, Level level) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.STATUS_CURRENT_REPETITION, repetitions);
        String where = Constants.STATUS_EXERCISE_ID + "=" + id + " AND " + level.getValue() + "=" + Constants.STATUS_LEVEL;
        dbWritable.update(Constants.DATABASE_STATUS_TABLE_NAME, contentValues, where, null);
    }

    public void dropAllTables() {
        dbWritable.setVersion(1);
        //Brisanje postojecih tablica
        List<String> tables = new ArrayList<String>();
        Cursor cursor = dbWritable.rawQuery("SELECT * FROM sqlite_master WHERE type='table';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String tableName = cursor.getString(1);
            if (!tableName.equals("android_metadata") &&
                    !tableName.equals("sqlite_sequence"))
                tables.add(tableName);
            cursor.moveToNext();
        }
        cursor.close();

        for (String tableName : tables) {
            dbWritable.execSQL("DROP TABLE IF EXISTS " + tableName);
        }

    }
}
