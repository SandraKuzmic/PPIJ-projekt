package jawas.ppij.fer.hr.trainme.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.activity.MainFragment;

public class SwipeAdapter extends FragmentPagerAdapter {
    private static final int PAGES = 2;
    private Context context;

    public SwipeAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return MainFragment.createFragment(position + 1);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(position == MainFragment.PAGE_DEFAULT - 1 ? R.string.daily_workout : R.string.workouts);
    }

    @Override
    public int getCount() {
        return PAGES;
    }
}
