package jawas.ppij.fer.hr.trainme.enums;

public enum Category {
    ARMS, CHEST, ABS,  BACK, LEGS, CARDIO
}
