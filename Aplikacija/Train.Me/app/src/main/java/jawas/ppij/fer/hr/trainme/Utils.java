package jawas.ppij.fer.hr.trainme;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jawas.ppij.fer.hr.trainme.db.Database;
import jawas.ppij.fer.hr.trainme.entity.Workout;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class Utils {

    public static String extractUrlId(String url) {
        Pattern p = Pattern.compile("watch\\?v=(.+)$");
        Matcher m = p.matcher(url);

        return m.find() ? m.group(1) : null;
    }

    public static Toast addToast(Toast toast, Context context, String msg, boolean isShort) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, msg, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
        toast.show();

        return toast;
    }


    public static Toast addToast(Toast toast, Context context, int id, boolean isShort) {
        String msg = context.getString(id);
        return addToast(toast, context, msg, isShort);
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    public static String capitalizeLetter(String text) {
        return text.substring(0, 1).toUpperCase()
                + text.substring(1).toLowerCase();
    }

    public static List<Workout> getWorkouts(Context context, int level) {
        //kopija metode iz MainActivitija
        /*LinkedList<Workout> workouts = new LinkedList<>();
        workouts.add(new Workout(1, "pushups", "Most basic chest exercise", "https://www.youtube.com/watch?v=JyCG_5l3XLk" , Level.BEGINNER, Category.CHEST, 0, 20));
        workouts.add(new Workout(2, "pullups", "King of the back exercise", "https://www.youtube.com/watch?v=Ir8IrbYcM8w" , Level.INTERMEDIATE, Category.BACK, 0, 15));
        workouts.add(new Workout(5, "V-ups", "Be victorious with V-ups", "https://www.youtube.com/watch?v=t6OC23JDQLU" , Level.INTERMEDIATE, Category.ABS, 0, 20));
        return workouts;*/

        Database db = new Database(context);
        //Uncomment below for purpose of testing on all workouts and commnet everything below that line
        //return db.getWorkoutsByLevel(level);
        List<Workout> listOfAllByLevel = db.getWorkoutsByLevel(level);
        List<Workout> listOfAllNoDuplicates = new ArrayList<>();

        for (Workout wrkt : listOfAllByLevel) {
            if (listOfAllNoDuplicates.contains(wrkt))
                continue;
            Workout greatestWorkout = wrkt;
            for (Workout workout : listOfAllByLevel)
                if (workout.getName().equals(wrkt.getName())
                        && workout.getLevel().getValue() > greatestWorkout.getLevel().getValue())
                    greatestWorkout = workout;
            listOfAllNoDuplicates.add(greatestWorkout);
        }
        return listOfAllNoDuplicates;
    }

}
