package jawas.ppij.fer.hr.trainme.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.databinding.ActivityLevelBinding;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class LevelActivity extends AppCompatActivity {

    private ActivityLevelBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level);

        OnLevelCheckedListener beginner = new OnLevelCheckedListener(Level.BEGINNER, mBinding.checkboxBeginner);
        mBinding.cvBeginner.setOnClickListener(beginner);
        mBinding.checkboxBeginner.setOnCheckedChangeListener(beginner);

        OnLevelCheckedListener intermediate = new OnLevelCheckedListener(Level.INTERMEDIATE, mBinding.checkboxIntermediate);
        mBinding.cvIntermediate.setOnClickListener(intermediate);
        mBinding.checkboxIntermediate.setOnCheckedChangeListener(intermediate);

        OnLevelCheckedListener advanced = new OnLevelCheckedListener(Level.ADVANCED, mBinding.checkboxAdvanced);
        mBinding.cvAdvanced.setOnClickListener(advanced);
        mBinding.checkboxAdvanced.setOnCheckedChangeListener(advanced);
    }

    private class OnLevelCheckedListener implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
        private Level level;
        private CheckBox checkBox;

        OnLevelCheckedListener(Level level, CheckBox checkBox) {
            this.level = level;
            this.checkBox = checkBox;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                addToSharedPrefs();
            }
        }

        @Override
        public void onClick(View v) {
            checkBox.setChecked(true);
        }

        private void addToSharedPrefs() {
            SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_workout), MODE_PRIVATE);
            preferences.edit().putInt(getString(R.string.pref_level), level.getValue()).apply();
            startActivity(new Intent(LevelActivity.this, MainActivity.class));
            finish();
        }
    }
}
