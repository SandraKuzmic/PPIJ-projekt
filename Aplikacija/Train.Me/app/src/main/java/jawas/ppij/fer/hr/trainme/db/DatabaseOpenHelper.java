package jawas.ppij.fer.hr.trainme.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import jawas.ppij.fer.hr.trainme.enums.Category;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class DatabaseOpenHelper extends SQLiteOpenHelper {

    static DatabaseOpenHelper getInstance(Context context, DatabaseOpenHelper databaseOpenHelper) {
        if (databaseOpenHelper == null) {
            databaseOpenHelper = new DatabaseOpenHelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        }
        return databaseOpenHelper;
    }

    private DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private void addWorkouts (SQLiteDatabase db) {

        //push ups 1
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME +  "(" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                1 + ", 'push ups', 'Body weight king for chest', 'https://www.youtube.com/watch?v=JyCG_5l3XLk');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                1 + ", " + 1 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", "  + 0 + ", "  + 20 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                2 + ", " + 1 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", "  + 0 + ", "  + 40 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                3 + ", " + 1 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", " + 0 + ", " + 60 + ");"
        );

        //pull ups 2
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                2 + ", 'pull ups', 'Pull yourself up', 'https://www.youtube.com/watch?v=Ir8IrbYcM8w');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                4 + ", " + 2 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.BACK.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                5 + ", " + 2 + ", " + Level.ADVANCED.getValue() + ", '" + Category.BACK.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );

        //dips 3
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                3 + ", 'dips', 'New way to use parallel bars', 'https://www.youtube.com/watch?v=dX_nSOOJIsE');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                6 + ", " + 3 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                7 + ", " + 3 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CHEST.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );

        //sit ups 4
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                4 + ",'sit ups', 'Nice way to start working on abs', 'https://www.youtube.com/watch?v=hYEnbuNy-h0');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                8 + ", " + 4 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                9 + ", " + 4 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 30 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                10 + ", " + 4 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 50 + ");"
        );

        //russian twist 5
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                5 + ", " + "'russian twist', 'From Russia...with love', 'https://www.youtube.com/watch?v=NeAtimSCxsY');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                11 + ", " + 5 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ABS.toString() + "', " + 12 + ", " + 0 + ", " + 20 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                12 + ", " + 5 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ABS.toString() + "', " + 18 + ", " + 0 + ", " + 20 + ");"
        );

        //deadlift 6
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                6 + ", 'deadlift', 'Must do for lower back', 'https://www.youtube.com/watch?v=DNJya2CBQJ8');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                13 + ", " + 6 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.BACK.toString() + "', " + 50 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                14 + ", " + 6 + ", " + Level.ADVANCED.getValue() + ", '" + Category.BACK.toString() + "', " + 80 + ", " + 0 + ", " + 10 + ");"
        );

        //jackknife 7
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                7 + ", 'jackknife', 'Crunches for the elite', 'https://www.youtube.com/watch?v=CWOZyf8kaH0');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                15 + ", " + 7 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 25 + ");"
        );

        //squat 8
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                8 + ", 'squat', 'Strong legs are a must', 'https://www.youtube.com/watch?v=UXJrBgI2RxA');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                16 + ", " + 8 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.LEGS.toString() + "', " + 50 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                17 + ", " + 8 + ", " + Level.ADVANCED.getValue() + ", '" + Category.LEGS.toString() + "', " + 70 + ", " + 0 + ", " + 10 + ");"
        );

        //pistol squat 9
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + "(" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values ("+
                9 + ", 'pistol squat', 'Completely legal weapon', 'https://www.youtube.com/watch?v=smn5zko8Kpk');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                18 + ", " + 9 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " (" +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                19 + ", " + 9 + ", " + Level.ADVANCED.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        //burpee 10
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                10 + ",'burpee', 'Army workout for the best', 'https://www.youtube.com/watch?v=I1bqF_WyLyU');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                20 + ", " + 10 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                21 + ", " + 10 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        //dumbbell bench press 11
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                11 + ",'dumbbell bench press', 'What to do with a bench and 2 dumbbells', 'https://www.youtube.com/watch?v=ZOwwBk642SI');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                22 + ", " + 11 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CHEST.toString() + "', " + 12 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                23 + ", " + 11 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CHEST.toString() + "', " + 18 + ", " + 0 + ", " + 10 + ");"
        );

        //flat dumbbell fly 12
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                12 + ",'flat bumbbell fly', 'You don`t need wings to fly', 'https://www.youtube.com/watch?v=LoFb_PBPl9c');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                24 + ", " + 12 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CHEST.toString() + "', " + 5 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                25 + ", " + 12 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CHEST.toString() + "', " + 8 + ", " + 0 + ", " + 10 + ");"
        );

        //barbell good morning 13
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                13 + ",'barbell good morning', 'You don`t have to do these only in the morning', 'https://www.youtube.com/watch?v=xUZRtvrJgSg');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                26 + ", " + 13 + ", " + Level.BEGINNER.getValue() + ", '" + Category.BACK.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                27 + ", " + 13 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.BACK.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                28 + ", " + 13 + ", " + Level.ADVANCED.getValue() + ", '" + Category.BACK.toString() + "', " + 20 + ", " + 0 + ", " + 10 + ");"
        );

        //barbell pullover 14
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                14 + ",'barbell pullover', 'What to do with a bench and 1 dumbbell', 'https://www.youtube.com/watch?v=zUVzVXMh9Nc');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                29 + ", " + 14 + ", " + Level.BEGINNER.getValue() + ", '" + Category.BACK.toString() + "', " + 5 + ", " + 0 + ", " + 15 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                30 + ", " + 14 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.BACK.toString() + "', " + 8 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                31 + ", " + 14 + ", " + Level.ADVANCED.getValue() + ", '" + Category.BACK.toString() + "', " + 10 + ", " + 0 + ", " + 8 + ");"
        );

        //dumbbell side bend 15
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                15 + ",'dumbbell side bend', 'For overall core strength', 'https://www.youtube.com/watch?v=dL9ZzqtQI5c');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                32 + ", " + 15 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ABS.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                33 + ", " + 15 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ABS.toString() + "', " + 15 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                34 + ", " + 15 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ABS.toString() + "', " + 20 + ", " + 0 + ", " + 10 + ");"
        );

        //diamond push ups 16
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                16 + ",'diamond push ups', 'Close stance for maximal arm involvement', 'https://www.youtube.com/watch?v=pD3mD6WgykM');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                35 + ", " + 16 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                36 + ", " + 16 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                37 + ", " + 16 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        //chin ups 17
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                17 + ",'chin ups', 'Arm killer, but also involves your back', 'https://www.youtube.com/watch?v=_71FpEaq-fQ');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                38 + ", " + 17 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 5 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                39 + ", " + 17 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                40 + ", " + 17 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );

        //barbell curl 18
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                18 + ",'barbell curl', 'Classic, simple and effective', 'https://www.youtube.com/watch?v=kwG2ipFRgfo');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                41 + ", " + 18 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ARMS.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                42 + ", " + 18 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ARMS.toString() + "', " + 15 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                43 + ", " + 18 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ARMS.toString() + "', " + 18 + ", " + 0 + ", " + 10 + ");"
        );

        //overhead shoulder press 19
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                19 + ",'overhead shoulder press', 'Strong arms require strong shoulders', 'https://www.youtube.com/watch?v=flpBXsHSVDk');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                44 + ", " + 19 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ARMS.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                45 + ", " + 19 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ARMS.toString() + "', " + 15 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                46 + ", " + 19 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ARMS.toString() + "', " + 20 + ", " + 0 + ", " + 20 + ");"
        );

        //lounge 20
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                20 + ",'lounge', 'Workout for every muscle in your legs', 'https://www.youtube.com/watch?v=QF0BQS2W80k');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                47 + ", " + 20 + ", " + Level.BEGINNER.getValue() + ", '" + Category.LEGS.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                48 + ", " + 20 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.LEGS.toString() + "', " + 20 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                49 + ", " + 20 + ", " + Level.ADVANCED.getValue() + ", '" + Category.LEGS.toString() + "', " + 30 + ", " + 0 + ", " + 10 + ");"
        );

        //hip bridge 21
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                21 + ",'hip bridge', 'Activate your core and thights', 'https://www.youtube.com/watch?v=MiVHq0EM234');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                50 + ", " + 21 + ", " + Level.BEGINNER.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                51 + ", " + 21 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.LEGS.toString() + "', " + 10 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                52 + ", " + 21 + ", " + Level.ADVANCED.getValue() + ", '" + Category.LEGS.toString() + "', " + 20 + ", " + 0 + ", " + 10 + ");"
        );

        //jump squat 22 Challenge yourself
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                22 + ",'jump squat', 'Challenge yourself', 'https://www.youtube.com/watch?v=qv3hoZqSk3c');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                53 + ", " + 22 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                54 + ", " + 22 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                55 + ", " + 22 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // jumping lounge 23
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                23 + ",'jumping lounge', 'Combine lounges and jumps', 'https://www.youtube.com/watch?v=y7Iug7eC0dk');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                56 + ", " + 23 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                57 + ", " + 23 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                58 + ", " + 23 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 30 + ");"
        );

        // mountain climbers 24
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                24 + ",'mountain climbers', 'When you want to climb, but mountain isn`t an option', 'https://www.youtube.com/watch?v=s2zB3tU1STA');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                59 + ", " + 24 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                60 + ", " + 24 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                61 + ", " + 24 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 30 + ");"
        );

        // incline push ups 25
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                25 + ",'incline push ups', 'Easier variation of a normal pushup', 'https://www.youtube.com/watch?v=bXsbK9UPu3c');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                62 + ", " + 25 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // incline dumbbell fly 26
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                26 + ",'incline dumbbell fly', 'Focus on shoulders and upper chest', 'https://www.youtube.com/watch?v=ajdFwa-qM98');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                63 + ", " + 26 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CHEST.toString() + "', " + 12 + ", " + 0 + ", " + 10 + ");"
        );

        // one arm push up 27
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                27 + ",'one arm push up', 'Push ups for the elite', 'https://www.youtube.com/watch?v=hbYQM1Ss2zY');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                64 + ", " + 27 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CHEST.toString() + "', " + 0 + ", " + 0 + ", " + 10 + ");"
        );

        // lower back extension 28
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                28 + ",'lower back extension', 'Excellent way to start training lower back', 'https://www.youtube.com/watch?v=Ikq3OCFoP7c');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                65 + ", " + 28 + ", " + Level.BEGINNER.getValue() + ", '" + Category.BACK.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // standing row 29
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                29 + ",'standing row', 'Not having a boat is not an excuse to skip rowing', 'https://www.youtube.com/watch?v=Ov5qMp4QPIA');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                66 + ", " + 29 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.BACK.toString() + "', " + 20 + ", " + 0 + ", " + 15 + ");"
        );

        // standing one arm row 30
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                30 + ",'standing one arm row', 'Not having a boat is not an excuse to skip rowing', 'https://www.youtube.com/watch?v=oSEYo6f3eWU');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                67 + ", " + 30 + ", " + Level.ADVANCED.getValue() + ", '" + Category.BACK.toString() + "', " + 20 + ", " + 0 + ", " + 15 + ");"
        );

        // scissors 31
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                31 + ",'scissors', 'Start sculpting lower abs', 'https://www.youtube.com/watch?v=WoNCIBVLbgY');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                68 + ", " + 31 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 30 + ");"
        );

        // bicycle kick 32
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                32 + ",'bicycle kick', 'Imagine yourself on tour de France', 'https://www.youtube.com/watch?v=UZZhuJACZJM');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                69 + ", " + 32 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 30 + ");"
        );

        // exercise ball jackknife 33
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                33 + ",'exercise ball jackknife', 'Push yourself a little bit more', 'https://www.youtube.com/watch?v=avRJLkvU6gA');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                70 + ", " + 33 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ABS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );

        // bench dip 34
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                34 + ",'bench dip', 'Easier variation of normal dip', 'https://www.youtube.com/watch?v=lPXJMzFXFvc');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                71 + ", " + 34 + ", " + Level.BEGINNER.getValue() + ", '" + Category.ARMS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );

        // hammer curl 35
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                35 + ",'hammer curl', 'Dumbbell curls without forearm rotation', 'https://www.youtube.com/watch?v=TwD-YGVP4Bk');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                72 + ", " + 35 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.ARMS.toString() + "', " + 15 + ", " + 0 + ", " + 10 + ");"
        );

        // french press 36
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                36 + ",'french press', 'Another great thing from France', 'https://www.youtube.com/watch?v=JImgCWzCHwI');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                73 + ", " + 36 + ", " + Level.ADVANCED.getValue() + ", '" + Category.ARMS.toString() + "', " + 15 + ", " + 0 + ", " + 10 + ");"
        );

        // frog squat 37
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                37 + ",'frog squat', 'Wide stance half squat for upper legs', 'https://www.youtube.com/watch?v=oN88JAT9MDE');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                74 + ", " + 37 + ", " + Level.BEGINNER.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // curtsy lunge 38
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                38 + ",'curtsy lunge', 'There is always an exercise which a little bit messed up', 'https://www.youtube.com/watch?v=9Nvhkjsxk40');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                75 + ", " + 38 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // prone hamstring curl 39
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                39 + ",'prone hamstring curl', 'Exercise for the entire core', 'https://www.youtube.com/watch?v=1mbtAVUkONY');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                76 + ", " + 39 + ", " + Level.ADVANCED.getValue() + ", '" + Category.LEGS.toString() + "', " + 0 + ", " + 0 + ", " + 15 + ");"
        );

        // jump rope 40
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                40 + ",'jump rope', 'One of the best cardio workouts for witch you do not need a lot of space', 'https://www.youtube.com/watch?v=c9q03dYf10Q');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                77 + ", " + 40 + ", " + Level.BEGINNER.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 20 + ");"
        );

        // 180 jump squat 41
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                41 + ",'180 jump squat', 'Shallow squat combined with jumps and tourns', 'https://www.youtube.com/watch?v=0hJYeVnzE50');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                78 + ", " + 41 + ", " + Level.INTERMEDIATE.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 12 + ");"
        );

        // run stance squat 42
        db.execSQL("insert into " + Constants.DATABASE_EXERCISE_TABLE_NAME + " (" +
                Constants.EXERCISE_ID + ", " +
                Constants.EXERCISE_NAME + ", " +
                Constants.EXERCISE_DESCRIPTION + ", " +
                Constants.EXERCISE_URL  + ") values (" +
                42 + ",'run stance squat', 'Normal squat combined with jumps and tourns', 'https://www.youtube.com/watch?v=fv66wRmbXqc');"
        );
        db.execSQL("insert into  " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + ", " +
                Constants.STATUS_EXERCISE_ID + ", " +
                Constants.STATUS_LEVEL + ", " +
                Constants.STATUS_CATEGORY + ", " +
                Constants.STATUS_WEIGHT + ", " +
                Constants.STATUS_CURRENT_REPETITION + ", " +
                Constants.STATUS_NEEDED_REPETITION  + ") values (" +
                79 + ", " + 42 + ", " + Level.ADVANCED.getValue() + ", '" + Category.CARDIO.toString() + "', " + 0 + ", " + 0 + ", " + 12 + ");"
        );

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + Constants.DATABASE_EXERCISE_TABLE_NAME + " ( " +
                        Constants.EXERCISE_ID + " integer primary key autoincrement," +
                        Constants.EXERCISE_NAME + " text not null," +
                        Constants.EXERCISE_DESCRIPTION + " text not null," +
                        Constants.EXERCISE_URL + " text" + " );"
        );
        db.execSQL("create table " + Constants.DATABASE_STATUS_TABLE_NAME + " ( " +
                Constants.STATUS_ID + " integer primary key autoincrement," +
                Constants.STATUS_EXERCISE_ID + " integer," +
                Constants.STATUS_LEVEL + " smallint not null," +
                Constants.STATUS_CATEGORY + " text not null," +
                Constants.STATUS_WEIGHT + " integer," +
                Constants.STATUS_CURRENT_REPETITION + " integer default 0," +
                Constants.STATUS_NEEDED_REPETITION + " integer not null," +
                "foreign key ( " + Constants.STATUS_EXERCISE_ID + " ) references " + Constants.DATABASE_EXERCISE_TABLE_NAME + "(" + Constants.EXERCISE_ID + ") "
                + " );"
        );

        addWorkouts (db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + Constants.DATABASE_EXERCISE_TABLE_NAME);
        db.execSQL("drop table if exists " + Constants.DATABASE_STATUS_TABLE_NAME);

        onCreate(db);
    }
}
