package jawas.ppij.fer.hr.trainme.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jawas.ppij.fer.hr.trainme.R;
import jawas.ppij.fer.hr.trainme.databinding.FragmentDailyWorkoutBinding;
import jawas.ppij.fer.hr.trainme.databinding.FragmentWorkoutsBinding;

public class MainFragment extends Fragment {
    private static final String PAGE_NUMBER = "page_number";
    public static final int PAGE_DEFAULT = 1;


    public MainFragment() {
    }

    public static MainFragment createFragment(int page) {
        MainFragment fragment = new MainFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(PAGE_NUMBER, page);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int page;
        try {
            page = getArguments().getInt(PAGE_NUMBER);
        } catch (Exception e) {
            page = PAGE_DEFAULT;
        }

        return page == PAGE_DEFAULT ? createDailyWorkout(inflater, container) : createWorkoutsList(inflater, container);
    }

    private View createDailyWorkout(LayoutInflater inflater, ViewGroup container) {
        FragmentDailyWorkoutBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_daily_workout, container, false);

        ((MainActivity) getActivity()).setDailyWorkout(binding);

        return binding.getRoot();
    }

    private View createWorkoutsList(LayoutInflater inflater, ViewGroup container) {
        FragmentWorkoutsBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_workouts, container, false);

        ((MainActivity) getActivity()).setWorkoutsList(binding);

        return binding.getRoot();
    }
}
