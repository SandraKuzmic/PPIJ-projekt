package jawas.ppij.fer.hr.trainme.entity;

import java.io.Serializable;

import jawas.ppij.fer.hr.trainme.enums.Category;
import jawas.ppij.fer.hr.trainme.enums.Level;

public class Workout implements Serializable {

    private int id;
    private String name;
    private String description;
    private String url;
    private Level level;
    private Category category;
    private int weight;
    private int currentReps;
    private int goalReps;

    public Workout() {
    }

    public Workout(int id, String name, String description, String url, Level level, Category category, int currentReps, int goalReps) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.level = level;
        this.category = category;
        this.currentReps = currentReps;
        this.goalReps = goalReps;
    }

    public Workout(int id, String name, String description, String url, Level level, Category category, int weight, int currentReps, int goalReps) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.level = level;
        this.category = category;
        this.weight = weight;
        this.currentReps = currentReps;
        this.goalReps = goalReps;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String naziv) {
        this.name = naziv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String opis) {
        this.description = opis;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getCurrentReps() {
        return currentReps;
    }

    public void setCurrentReps(int currentReps) {
        this.currentReps = currentReps;
    }

    public int getGoalReps() {
        return goalReps;
    }

    public void setGoalReps(int goalReps) {
        this.goalReps = goalReps;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isGoalAccomplished() {
        return currentReps >= goalReps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workout workout = (Workout) o;

        return id == workout.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", level=" + level +
                ", category=" + category +
                ", weight=" + weight +
                ", currentReps=" + currentReps +
                ", goalReps=" + goalReps +
                '}';
    }
}
