package jawas.ppij.fer.hr.trainme.db;

public class Constants {
    static final String DATABASE_NAME = "trainMe_database.db";
    static final String DATABASE_EXERCISE_TABLE_NAME = "exercise";
    static final String DATABASE_STATUS_TABLE_NAME = "status";
    static final int DATABASE_VERSION = 5;

    static final String EXERCISE_ID = "id";
    static final String EXERCISE_NAME = "name";
    static final String EXERCISE_DESCRIPTION = "description";
    static final String EXERCISE_URL = "url";

    static final String STATUS_ID = "id";
    static final String STATUS_EXERCISE_ID = "idExercise";
    static final String STATUS_LEVEL = "level";
    static final String STATUS_CATEGORY = "category";
    static final String STATUS_WEIGHT = "weight";
    static final String STATUS_CURRENT_REPETITION = "currentRepetition";
    static final String STATUS_NEEDED_REPETITION = "neededRepetition";

}
