Enumi:
	Levels: Beginner, Intermediate, Advanced
	Kategorije: Arms, Chest, Abs, Back, Legs, Cardio
		- svaka kategorija ima ukupno 9 vježbi (3 za svaki level)
 
Vježba sadrži:
 - id
 - naziv
 - opis
 - url

Status sadrži:
 - id
 - id vjezbe
 - level
 - kilaza
 - trenutni broj ponavljanja
 - potreban broj ponavljanja 
 
Tablice u bazi:
- Vježba
- Status


