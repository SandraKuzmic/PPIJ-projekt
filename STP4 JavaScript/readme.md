Prezentacija 
STP4-Uvod_u_JavaScript.ppt ili .pdf

Primjeri za prezentaciju
JavaScript - examples



Pokretanje primjera
 - dovoljno je samo double-click na .html file

Uredivanje i dodavanje novih primjera
 - dodati novi html i js file

 
Dodatak - pokretanje i uređivanje iz intellij-a (npr ako želite autocomplete funkcija)
 - New Project > Web > Static Web
 - Dodati ime.html file po uzoru na test.html
 - Dodati ime.js file za pisanje js koda
 - U ime.html staviti tag <script src = "ime.js">
 - Pokretanje jednostavno Run nakon što je prethodno odabran ime.html > otvara se tab u browseru
 - Nakon svake promjene u js file-u dosta je samo refreshat postojeći tab