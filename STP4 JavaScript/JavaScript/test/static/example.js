class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  static distance(a, b) {
    const dx = a.x - b.x;
    const dy = a.y - b.y;

    return Math.hypot(dx, dy);
  }
}

const p1 = new Point(5, 5);
const p2 = new Point(10, 10);

write(Point.distance(p1, p2)); // 7.0710678118654755





//pozivanje statičke metode u statičkoj metodi
class StaticMethodCall {
  static staticMethod() {
    return 'Static method has been called';
  }
  static anotherStaticMethod() {
    return this.staticMethod() + ' from another static method';
  }
}
write(StaticMethodCall.staticMethod()); 

write(StaticMethodCall.anotherStaticMethod()); 





function write(string) {
    document.write(string + "<br/>")
}