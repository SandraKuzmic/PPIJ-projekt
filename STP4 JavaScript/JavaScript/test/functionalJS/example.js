function simple() {

    let numbers = [0, 1, 2, 3, 4, 5];
    write(`Numbers ${numbers}`);

    //example with function
    let doubled = numbers.map(function (n) {
        return n * 2
    });
    write(`Doubled numbers ${doubled}`);


    //example with arrow functions
    let even = numbers.filter(n => n % 2 === 0);
    write(`Even numbers ${even}`);

    let sum = numbers.reduce((prev, next) => prev + next, 0); //0 is accumulator variable at first iteration
    write(`Sum of numbers ${sum}`);

}

function complex() {

    let courses = [
        {name: "PPIJ", ects: 4},
        {name: "RPPP", ects: 4},
        {name: "TP", ects: 2},
        {name: "OPP", ects: 8},
        {name: "Zavrsni", ects: 12}
    ];

    let aboveFourEcts = courses
        .map(course => course.ects)
        .filter(ects => ects >= 4)
        .reduce((prev, next) => prev + next, 0);

    write(`Sum of ects ${aboveFourEcts}`)

}


simple();
complex();


function write(string) {
    document.write(string + "<br/>")
}
