//object attributes name must match function parameters
function printPerson({firstName, lastName, age, city = "London"}) {
    document.write(`${firstName} ${lastName} (${age}), ${city} <br/>`)
}

//destructing in arrow functions
const getAge = ({age}) => age;

//declaring object
const person = {
    firstName: "Nick",
    middleName: "James",
    lastName: "Anderson",
    age: 35,
    sex: "M"
};

printPerson(person);
document.write(getAge(person));

//destructing syntax
//new variable age and surname are created
const {age, lastName: surname} = person;
document.write(`${surname}, ${age}`);

//this won't work since it is from object person
document.write(`${lastName}, ${sex}`);