//object attributes name must match function parameters
function printTwoNumbers([x, y, z = 5]) {
    document.write(`${x}, ${y} and ${z} <br/>`)
}

//destructing in arrow functions
const getNumber = ([x]) => x;

//declaring object
const numbers = [1, 2];

printTwoNumbers(numbers);
document.write(getNumber(numbers));
