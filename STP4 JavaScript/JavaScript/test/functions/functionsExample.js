//traditional function with explicit return
function double1(x) {
    return x * 2;
}

//arrow function with explicit return
const double2 = x => {
    return x * 2
};

//arrow function with implicit return
const double3 = x => x * 2;

document.write(double1(2)+ "</br>");
document.write(double2(2)+ "</br>");
document.write(double3(2)+ "</br>");


//saving functions in variable for redefining
var calculate = function (a, b) {
    return a + b;
};
document.write(calculate(10, 6) + "</br>"); //prints 16

calculate = (a, b) => a - b;
document.write(calculate(10, 6)+ "</br>"); //prints 4


//arguments
const add0 = () => 2 + 5;
const add1 = x => x + x;
const add2 = (x, y) => x + y;

