//class declarations
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
  // Getter
  get area() {
    return this.calcArea();
  }
  // Method
  calcArea() {
    return this.height * this.width;
  }
}




//class expressions

    // unnamed
    var Rectangle2 = class {
      constructor(height, width) {
        this.height = height;
        this.width = width;
      }

      get area() {
        return this.calcArea();
      }
      // Method
      calcArea() {
        return this.height * this.width;
      }
    };

    // named
    var Rectangle3 = class Rectangle {
      constructor(height, width) {
        this.height = height;
        this.width = width;
      }

      get area() {
        return this.calcArea();
      }
      // Method
      calcArea() {
        return this.height * this.width;
      }
    };


const square1 = new Rectangle(10, 10);
const square2 = new Rectangle2(10, 10);
const square3 = new Rectangle3(10, 10);

document.write(square1.area + '<br/>'); // 100
document.write(square2.area + '<br/>'); // 100
document.write(square3.area + '<br/>'); // 100