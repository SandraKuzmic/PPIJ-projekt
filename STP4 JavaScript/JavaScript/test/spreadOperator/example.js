//allows an iterable to be expanded in places where zero or more arguments are expected
function sum(x, y, z) {
  return x + y + z;
}

const numbers = [1, 2, 3];
write(sum(...numbers));


//create a new array using an existing array as one part of it
let parts = ['shoulders', 'knees']; 
let lyrics = ['head', ...parts, 'and', 'toes']; 
write(lyrics);


//merge objects
let obj1 = { foo: 'bar', x: 42 };
let obj2 = { foo: 'baz', y: 13 };
let mergedObj = { ...obj1, ...obj2 };
write('foo: ' + mergedObj.foo);
write('x: ' + mergedObj.x);
write('y: ' + mergedObj.y);

function write(string) {
    document.write(string + "<br/>")
}