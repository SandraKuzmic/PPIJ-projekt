let courses = [
    {name: "PPIJ", year: 3, ects: 4},
    {name: "RPPP", year: 3, ects: 4},
    {name: "VJEKOM", year: 1, ects: 3},
    {name: "OPP", year: 3, ects: 8},
    {name: "VIS", year: 2, ects: 5},
    {name: "OE", year: 1, ects: 6}
];

//lets calculate boringSum of all ects on third year
let boringSum = 0;
for (let i in courses) {
    let c = courses[i];
    if (c.year === 3) {
        boringSum += c.ects;
    }
}
write(`Sum of ects ${boringSum}`);      //prints 16

//lets do the same using functional js
let functionalSum = courses
    .filter(course => course.year === 3)
    .map(course => course.ects)
    .reduce((prev, next) => prev + next, 0);

write(`Sum of ects ${functionalSum}`);  //prints 16


//helper function
function write(string = "") {
    document.write(string + "<br/>")
}