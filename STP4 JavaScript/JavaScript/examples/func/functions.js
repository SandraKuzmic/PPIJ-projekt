//object car
let car = {
    brand: "Rimac",
    model: "c_2",
    hp: 1914,
    type:  "electric",
    info: function() {
        return this.brand + " " + this.model;
    }
};

write(car.info());        //Rimac c_2
write();

//arrow function for getting car properties
const getProperty = (property) => car[property];

for (let p in car) {
    write(`${p}: ${getProperty(p)}`);   //brand: Rimac model: c_2 hp: 1914 ...
}
write();

//destructing and default parameters
function printCarData({model, color = "silver"}) {
    write(model + " " + color);
}

printCarData(car);      //c_2 silver
write();

//mutable objects
let anotherCar = car;
anotherCar.brand = "Toyota";

write(`Original Car: ${car.brand}`);        //Toyota
write(`Another car: ${anotherCar.brand}`);  //Toyota
write();


//helper function
function write(string = "") {
    document.write(string + "<br/>")
}
