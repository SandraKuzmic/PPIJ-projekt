function blockScope () {
    let name = "Sandra";

    if (true) {
        let name = "Martina";
        write(name); //Martina
        // actually, name being block scoped, we just created new variable name, independent from previous
    }

    write(name); //Sandra
}

blockScope();

//name is not accessible outside the function
// document.write(name);

function accessibility() {
    write(number); //will raise Exception -> inspect in browser to see

    let number = 25;

    write(number); //will not be executed because of exception
}

accessibility();

//helper functions
function write(string) {
    document.write(string + "<br/>")
}

