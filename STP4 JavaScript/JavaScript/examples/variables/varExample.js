function functionScope () {
    var name = "Sandra";

    if (true) {
        var name = "Martina";
        write(name); //Martina
        // actually, name being function scoped, we just erased the previous name value "Sandra" for "Martina"
    }

    write(name); //Martina
}

functionScope();

// name is not accessible outside the function
// document.write(name);

function accessibility() {
    write(number); //will return undefined

    var number = 25;

    write(number); //will write 25

    //actually happen: var hoisting
    //var number
    //document.write(number)
    //number = 25
}

accessibility();

//helper functions
function write(string) {
    document.write(string + "<br/>")
}
