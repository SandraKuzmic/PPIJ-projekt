https://mbeaudru.github.io/modern-js-cheatsheet/

- features from: ES2015, often called ES6 -> u projektu je ECMAScript6 ECMAScript2015, 2016, 2017

-----------------------------------------------------------------------------------------------------------------------------------

- var 	-> function scoped (ako unutar fcije ima više varijabli deklariranih s var i istog su imena, jedna drugu prebrišu)
		-> var hoisting (declaring a variable anywhere in the code is equivalent to declaring it at the top
						This also means that a variable can appear to be used before it's declared)
		-> pr: varExample.js
	  
- let 	-> block scoped
		-> not accessible before they are assigned
		-> can’t be re-declared in the same scope (= dodati varijablu istog imena)
		-> pr. letExample.js
	  
- const -> block scoped
		-> not accessible before they are assigned
		-> can’t be re-declared in the same scope (= dodati varijablu istog imena)
		-> can't be reassigned (= promijeniti vrijednost postojećoj varijabli)
		
-----------------------------------------------------------------------------------------------------------------------------------

za funkcije općenito navesti da se može postaviti defaultna vrijednost parametra koja se primjenjuje u slučaju da:
	- fciji nismo proslijedili parametar
	- proslijeđeni parametar iznosi undefined 
** ako proslijedimo null nece se primjeniti defaultni parametar
	-> defaultParameter.js

Arrow functions
	- one argument of function can be put without brackets -> const add = x => x + x;
	- multiple arguments of function must be put in brackets -> const add = (x, y) => x + y; 
	- no arguments must be put in brackets -> const add = () => 2+5
	
	- reference this is taken from enclosing context without need for doing var that = this trick
		(primjer za to ce biti u onim globalnim primjerima, a ne na slajd) 
	-> functionExample.js	

-----------------------------------------------------------------------------------------------------------------------------------

Destructuring
 - objects 	-> eksplicitno specificiramo koje vrijednosti želimo uzeti
			-> možemo navesti i defaultne vrijednosti za slučaj da predani objekt nema navedeni atribut
			-> person.age <=> {age} = person 
			-> pr. objects.js
			
 - arrays  	-> eksplicitno specificiramo članove niza koje želimo uzeti
 			-> možemo navesti i defaultne vrijednosti za slučaj da predani niz nema navedenu vrijednost
			-> numbers[0] <=> [x] = numbers (uoči zagrade!!!)
			-> pr. arrays.js
			
-----------------------------------------------------------------------------------------------------------------------------------

Functional JS
 - omogućuje izbjegavanje for/forEach petlja
 - map() takes an array, does something on its elements and returns an array with the transformed elements.
 - filter() takes an array, decides element by element if it should keep it or not and returns an array with the kept elements only
 - reduce() takes an array and aggregates the elements into a single value (which is returned)
 
-----------------------------------------------------------------------------------------------------------------------------------

SPREAD OPERATOR

- omogućuju proslijeđivanje 0 ili više argumenata
	- funkcijama: myFunction(...iterableObj)
	- elementima (npr. poljima): [...iterableObj, '4', 'five', 6];
	- objektima: let objClone = { ...obj };

---------------------------------------------------------------------------------------------------------------------------------------------

IMPORT
import defaultExport from "module-name";
import * as name from "module-name";
import { export } from "module-name";
import { export as alias } from "module-name";
import { export1 , export2 } from "module-name";
import { export1 , export2 as alias2 , [...] } from "module-name";
import defaultExport, { export [ , [...] ] } from "module-name";
import defaultExport, * as name from "module-name";
import "module-name";


EXPORT
export { name1, name2, …, nameN };
export { variable1 as name1, variable2 as name2, …, nameN };
export let name1, name2, …, nameN; // also var
export let name1 = …, name2 = …, …, nameN; // also var, const
export function FunctionName(){...}
export class ClassName {...}

export default expression;
export default function (…) { … } // also class, function*
export default function name1(…) { … } // also class, function*
export { name1 as default, … };

export * from …;
export { name1, name2, …, nameN } from …;
export { import1 as name1, import2 as name2, …, nameN } from …;
export { default } from …;

----------------------------------------------------------------------------------------------------------------------------------------

KLASE
 - "specijalne funkcije" 
 - class expressions
 - class declarations

-------------------------------------------------------------------------------------------------------------------------------------------

STATIC
 - takve metode se pozivaju direktno na klasi te se ne mogu pozivati na instancama klase
